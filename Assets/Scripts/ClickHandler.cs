﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickHandler : MonoBehaviour {

	public GameObject blackSphere;
	public Button btnDropBall;

	// Use this for initialization
	void Start () {
		btnDropBall.onClick.AddListener (OnClick);
	}
	
	public void OnClick() {
		//Debug.Log (Random.Range(1, 1000));
		Instantiate (blackSphere, new Vector3(-0.5f+Random.Range(0.1f, 0.4f), 5f, -3f+Random.Range(0.1f, 0.4f)),Quaternion.identity);
	}
}
